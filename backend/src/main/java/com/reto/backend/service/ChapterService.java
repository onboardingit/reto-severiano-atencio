package com.reto.backend.service;

import com.reto.model.Chapter;
import com.reto.model.Character;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class ChapterService {

    public List<Chapter> getChapters(String sort, String bookName) {

        List<Chapter> chapters = businessChapterRequest(sort, bookName);

        return chapters;

    }

    private List<Chapter> businessChapterRequest(String sort, String bookName) {

        String url = "http://severiano-business:8088/api/chapter/"+sort;

        RestTemplate restTemplate = new RestTemplate();

        ChapterDTO chapterDTO = new ChapterDTO();
        chapterDTO.setSort(sort);
        chapterDTO.setBookName(bookName);

        HttpEntity<ChapterDTO> request = new HttpEntity<>(chapterDTO);

        ResponseEntity<List<Chapter>> chapterResponse =
                restTemplate.exchange(url,
                        HttpMethod.POST, request, new ParameterizedTypeReference<List<Chapter>>() {
                        });

        return chapterResponse.getBody();

    }

}
