package com.reto.backend.service;

import com.reto.model.Movie;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class MovieService {

    public List<Movie> getMovies() {

        List<Movie> movieRequest = businessMovieRequest();

        return movieRequest;

    }

    private List<Movie> businessMovieRequest() {

        String url = "http://severiano-business:8088/api/movie";

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<List<Movie>> movieResponse =
                restTemplate.exchange(url,
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Movie>>() {
                        });

        return movieResponse.getBody();
    }

}
