package com.reto.backend.service;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ChapterDTO {

    private String sort;

    private String bookName;

}
