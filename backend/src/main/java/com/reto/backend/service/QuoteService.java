package com.reto.backend.service;

import com.reto.model.Movie;
import com.reto.model.Quote;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class QuoteService {

    public List<Quote> getQuotes(String sort) {

        List<Quote> quotes = businessQuoteRequest(sort);

        return quotes;

    }

    private List<Quote> businessQuoteRequest(String sort) {

        //String url = "http://localhost:8088/api/quote/"+sort;
        String url = "http://severiano-business:8088/api/quote/"+sort;

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<List<Quote>> quoteResponse =
                restTemplate.exchange(url,
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Quote>>() {
                        });

        return quoteResponse.getBody();
    }
}
