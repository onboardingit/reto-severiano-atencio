package com.reto.backend.service;

import com.reto.model.Character;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CharacterService {

    public List<Character> getCharacters() {

        List<Character> charactersRequest = businessCharacterRequest();

        return charactersRequest;

    }

    private List<Character> businessCharacterRequest() {

        String url = "http://severiano-business:8088/api/character";

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<List<Character>> characterResponse =
                restTemplate.exchange(url,
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Character>>() {
                        });

        return characterResponse.getBody();
    }

}
