package com.reto.backend.controller;

import com.reto.api.QuoteApi;
import com.reto.backend.service.QuoteService;
import com.reto.model.Quote;
import com.reto.model.QuoteRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@RestController
@EnableSwagger2
@RequestMapping("/quote")
public class QuoteController implements QuoteApi {

    @Autowired
    private QuoteService quoteService;

    @PostMapping
    public ResponseEntity<List<Quote>> quote(@RequestBody QuoteRequest quoteRequest) {

        return ResponseEntity.ok(quoteService.getQuotes(quoteRequest.getSort()));

    }

}
