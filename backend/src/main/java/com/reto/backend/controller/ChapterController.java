package com.reto.backend.controller;

import com.reto.api.ChapterApi;
import com.reto.backend.service.ChapterService;
import com.reto.model.Chapter;
import com.reto.model.ChapterRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.validation.Valid;
import java.util.List;

@RestController
@EnableSwagger2
@RequestMapping("/chapter")
public class ChapterController implements ChapterApi {

    @Autowired
    private ChapterService chapterService;

    @PostMapping
    public ResponseEntity<List<Chapter>> chapter(@Valid ChapterRequest chapterRequest) {
        return ResponseEntity.ok(chapterService.getChapters(chapterRequest.getSort(), chapterRequest.getBookName()));
    }
}
