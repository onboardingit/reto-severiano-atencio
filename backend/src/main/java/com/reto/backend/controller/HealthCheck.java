package com.reto.backend.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
@RequestMapping("/health-check")
public class HealthCheck {

    @GetMapping
    public ResponseEntity healthCheck(){

        return ResponseEntity.ok("This is fine");

    }

}


