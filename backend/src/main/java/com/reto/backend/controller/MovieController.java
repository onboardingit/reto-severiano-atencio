package com.reto.backend.controller;

import com.reto.api.MovieApi;
import com.reto.backend.service.MovieService;
import com.reto.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@RestController
@EnableSwagger2
@RequestMapping("/movie")
public class MovieController implements MovieApi {

    @Autowired
    private MovieService movieService;

    @PostMapping
    public ResponseEntity<List<Movie>> movie() {

        return ResponseEntity.ok(movieService.getMovies());

    }

}
