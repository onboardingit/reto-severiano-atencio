package com.reto.business.controller;

import com.reto.api.CharacterApi;
import com.reto.business.service.CharacterService;
import com.reto.model.Character;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@RestController
@EnableSwagger2
@RequestMapping("/character")
public class CharacterController implements CharacterApi {

    @Autowired
    private CharacterService characterService;

    @GetMapping
    public ResponseEntity<List<Character>> character() {

        return ResponseEntity.ok(characterService.getCharacter());

    }

}