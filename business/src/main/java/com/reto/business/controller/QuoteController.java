package com.reto.business.controller;

import com.reto.api.QuoteApi;
import com.reto.business.service.QuoteService;
import com.reto.model.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@RestController
@EnableSwagger2
@RequestMapping("/quote")
public class QuoteController implements QuoteApi {

    @Autowired
    private QuoteService quoteService;

     @GetMapping("/{sort}")
    public ResponseEntity<List<Quote>> quote(@PathVariable String sort) {

         return ResponseEntity.ok(quoteService.getQuote(sort));

    }
}
