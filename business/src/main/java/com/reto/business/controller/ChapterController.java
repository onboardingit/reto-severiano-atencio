package com.reto.business.controller;

import com.reto.api.ChapterApi;
import com.reto.business.service.ChapterService;
import com.reto.model.Chapter;
import com.reto.model.ChapterRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@RestController
@EnableSwagger2
@RequestMapping("/chapter")
public class ChapterController implements ChapterApi {

    @Autowired
    private ChapterService chapterService;

    @PostMapping("/{sort}")
    public ResponseEntity<List<Chapter>> chapter(
            @PathVariable("sort") String sort,
            @RequestBody ChapterRequest chapterRequest) {

        return ResponseEntity.ok(chapterService.getChapter(sort, chapterRequest.getBookName()));

    }

}
