package com.reto.business.service;

import com.reto.business.entity.ChapterDTO;
import com.reto.model.Chapter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ChapterService {

    private final Map<String, String> books = new HashMap<>();

    public ChapterService() {
        books.put("5cf5805fb53e011a64671582", "The Fellowship Of The Ring");
        books.put("5cf58077b53e011a64671583", "The Two Towers");
        books.put("5cf58080b53e011a64671584", "The Return Of The King");
    }

    public List<Chapter> getChapter(String sort, String bookName) {

        String bookId = "";

        Optional<String> boodIdCheck = books.entrySet()
                .stream()
                .filter(e -> bookName.equals(e.getValue()))
                .map(Map.Entry::getKey)
                .findFirst();

        if (boodIdCheck.isEmpty()) {
            throw new IllegalStateException("Nombre del libro no es correcto");
        }

        bookId = boodIdCheck.get();

        ChapterDTO chapterDTO = externalRequestChapter(sort, bookId);

        return  chapterDTO.getDocs();

    }


    private ChapterDTO externalRequestChapter(String sort, String bookId) {

        if (bookId.equals("")) {

            throw new IllegalStateException("Book id is empty");

        }

        String url = "https://the-one-api.dev/v2/book/"+ bookId+"/chapter?sort=chapterName:"+sort;

        String accessToken = "1oaGUJnGT47yChFvjr-y";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer "+accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate.exchange(url, HttpMethod.GET, entity, ChapterDTO.class).getBody();

    }
}
