package com.reto.business.service;

import com.reto.business.entity.CharacterDTO;
import com.reto.model.Character;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CharacterService {

    public List<Character> getCharacter(){

        CharacterDTO characterDTO = externalRequestCharacter();

        return characterDTO.getDocs();

    }

    private CharacterDTO externalRequestCharacter() {

        String url = "https://the-one-api.dev/v2/character?limit=100&sort=name:desc";

        String accessToken = "1oaGUJnGT47yChFvjr-y";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer "+accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate.exchange(url, HttpMethod.GET, entity, CharacterDTO.class).getBody();

    }
}
