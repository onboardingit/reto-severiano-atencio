package com.reto.business.service;

import com.reto.business.entity.MovieDTO;
import com.reto.model.Movie;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class MovieService {

    public List<Movie> getMovies(){

        MovieDTO movieDTO = externalRequestMovies();

        return movieDTO.getDocs();

    }

    private MovieDTO externalRequestMovies() {

        String url = "https://the-one-api.dev/v2/movie?sort=name:asc";

        String accessToken = "1oaGUJnGT47yChFvjr-y";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer "+accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        System.out.println(restTemplate.exchange(url, HttpMethod.GET, entity, MovieDTO.class).getBody());

        return restTemplate.exchange(url, HttpMethod.GET, entity, MovieDTO.class).getBody();

    }
}
