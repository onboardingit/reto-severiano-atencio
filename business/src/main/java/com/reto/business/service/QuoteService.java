package com.reto.business.service;

import com.reto.business.entity.CharacterDTO;
import com.reto.business.entity.MovieDTO;
import com.reto.business.entity.QuoteDTO;
import com.reto.model.Quote;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class QuoteService {

    private final String accessToken = "1oaGUJnGT47yChFvjr-y";
    private final RestTemplate restTemplate = new RestTemplate();

    public List<Quote> getQuote(String sort) {

        QuoteDTO quoteDTO = externalRequestQuote(sort);

        MovieDTO movieDTO = externalRequestMovies();

        CharacterDTO characterDTO = externalRequestCharacter();

        QuoteDTO setedQuoted = setMovieAndCharacterNameQuotes(quoteDTO, movieDTO, characterDTO);

        return setedQuoted.getDocs();

    }

    private QuoteDTO setMovieAndCharacterNameQuotes(QuoteDTO quotes, MovieDTO movies, CharacterDTO characters) {

        if (movies != null){
            movies.getDocs().forEach(movie -> {
                quotes.getDocs().stream()
                        .filter(quote -> quote.getMovie().equals(movie.getId()))
                        .forEach(quote -> {
                            quote.setMovie(movie.getName());
                        });
            });
        }

        if ( characters != null){
            characters.getDocs().forEach(character -> {
                quotes.getDocs().stream()
                        .filter(quote -> quote.getCharacter().equals(character.getId()))
                        .forEach(quote -> {
                            quote.setCharacter(character.getName());
                        });
            });
        }

        return quotes;
    }

    private QuoteDTO externalRequestQuote(String sort) {

        String url = "https://the-one-api.dev/v2/quote?sort=character:"+sort+"&limit=25";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer "+accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate.exchange(url, HttpMethod.GET, entity, QuoteDTO.class).getBody();

    }

    private MovieDTO externalRequestMovies() {

        String url = "https://the-one-api.dev/v2/movie";

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer "+accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate.exchange(url, HttpMethod.GET, entity, MovieDTO.class).getBody();

    }

    private CharacterDTO externalRequestCharacter() {

        String url = "https://the-one-api.dev/v2/character";

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer "+accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate.exchange(url, HttpMethod.GET, entity, CharacterDTO.class).getBody();

    }

}
