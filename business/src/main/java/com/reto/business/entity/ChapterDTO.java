package com.reto.business.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.reto.model.Chapter;
import com.reto.model.Character;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Data
@Getter
@Setter
public class ChapterDTO {

    @JsonProperty("docs")
    private List<Chapter> docs = null;

    @JsonProperty("total")
    private BigDecimal total = null;

    @JsonProperty("limit")
    private BigDecimal limit = null;

    @JsonProperty("offset")
    private BigDecimal offset = null;

    @JsonProperty("page")
    private BigDecimal page = null;

    @JsonProperty("pages")
    private BigDecimal pages = null;

}
