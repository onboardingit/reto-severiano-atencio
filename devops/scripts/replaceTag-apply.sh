#!/bin/bash
manifest_name=$1
build_number=$2
branch=$3

tag="build.$branch.$build_number"
manifest_path="devops/k8s/$manifest_name"

sed -i "s/TAG/$tag/" $manifest_path
cat $manifest_path

kubectl apply -f $manifest_path
